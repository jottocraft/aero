# Aero

Aero is a simple software distributor.

## API

Base URL: `https://aero.jottocraft.com/<product identity>/<artifact type>`

*Providing a channel name serves the latest version in that channel*


### Artifact types

- `msix` Windows MSIX (only supports msixbundle, no optional or related packages)
    - `/ms-appinstaller` Redirects to a ms-appinstaller URI for installation via App Installer
    - `/<product identity>.appinstaller`
    - `/<product identity>.msixbundle`
    - `/Dependencies/<filename>`
- `expo` Expo JavaScript OTA updates
    - `/protocol1` Endpoint for requests made via the [Expo Updates v1 Protocol](https://docs.expo.dev/technical-specs/expo-updates-1), like u.expo.dev, this also optionally accepts runtime-version, channel-name, and platform via query parameters
    - `/assets/<asset key>` Gets an asset
    - `/<manifest ID>` Gets a specific update by manifest version
- Win32 Squirrel (soon)
- Android APK (soon)


## Uploading a deployment

- MSIX
    - Drag the .appinstaller and index.html files into the deployment folder
    - Rename .{appinstaller|appxsym|cer|msixbundle} files to <Product Identity>.{appinstaller|appxsym|cer|msixbundle}
    - Upload the deployment folder to `/<product identity>/msix/<deployment folder>` in R2
    - Create a release in Sanity and set the folder parameter to the name of the uploaded deployment folder
    - Profit
- Expo
    - Drag all assets and manifests into `/<product identity>/expo`. Assets should be named after their key (with no extension), and manifests should be named in the format `<manifest ID>.json`.


Copyright (c) jottocraft 2023