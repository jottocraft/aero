// Aero
// Copyright (c) jottocraft 2023

// Note: MSFT Delivery Optimization (aka the thing that actually requests and installs MSIX served by us)
// hits R2 HARD with Range-d requests when installing (I'm talking 300+ REQUESTS PER MINUTE from a single client...)
// MAKE SURE RANGED REQUESTS WORK AND ARE EFFICIENT!!! OTHERWISE INFRA WILL NOT BE HAPPY!!!

export async function serveR2(request: Request, env: Env, ctx: ExecutionContext, key: string, cacheControl?: string, contentType?: string) {
    if (request.method === "HEAD") {
        const object = await env.ARTIFACTS.head(key);
        if (object === null) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

        const headers = new Headers();
        object.writeHttpMetadata(headers);
        headers.set("Etag", object.httpEtag);
        headers.set("Content-Type", contentType ?? "application/octet-stream");
        if (cacheControl) headers.set("Cache-Control", cacheControl);
        headers.set("Accept-Ranges", "bytes");
        headers.set("Content-Length", String(object.size));

        return new Response(null, {
            status: 200,
            headers
        });
    }

    const hasRequestedRange = request.headers.get("Range") !== null;

    const object = await env.ARTIFACTS.get(key, {
        range: request.headers,
        onlyIf: request.headers
    }) as (R2ObjectBody | null);

    if (object === null) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    const headers = new Headers();
    object.writeHttpMetadata(headers);
    headers.set("ETag", object.httpEtag);
    headers.set("Content-Type", contentType ?? "application/octet-stream");
    if (cacheControl) headers.set("Cache-Control", cacheControl);
    headers.set("Accept-Ranges", "bytes");

    if (hasRequestedRange && object.range) {
        if (("offset" in object.range) || ("length" in object.range)) {
            headers.set("Content-Range", `bytes ${object.range.offset ?? 0}-${object.range.length !== undefined ? (object.range.offset ?? 0) + (object.range.length - 1) : object.size - 1}/${object.size}`);
        } else if ("suffix" in object.range) {
            headers.set("Content-Range", `bytes ${object.size - object.range.suffix}-${object.size - 1}/${object.size}`);
        }
    }

    return new Response(object.body ? await object.arrayBuffer() : null, {
        status: object.body ? (hasRequestedRange ? 206 : 200) : 304,
        headers
    });
}