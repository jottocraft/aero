// Aero
// Copyright (c) jottocraft 2023

export function isSafe(str: string) {
    return /^[A-Za-z0-9-_\.]*$/.test(str);
}