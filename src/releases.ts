// Aero
// Copyright (c) jottocraft 2023

import { wrapRatatoskrBinding } from "@jottocraft/ratatoskr-sdk";
import { createClient } from "@sanity/client";

// TODO: This shouldn't be hard-coded
function getSkuRequirement(release: Release): null | string[] {
    if ((release.product.identity === "prism") && (release.channel === "techpreview")) return ["PRISM_EAP_TECHPREVIEW"];
    return null;
}

async function getWithKVCache<T>(getter: () => T, env: Env, ctx: ExecutionContext, cacheKey: string): Promise<T> {
    const cachedData = await env.CACHE.get<T>(cacheKey, "json");
    if (cachedData) return cachedData;
    const data = await getter();
    if (data !== undefined) ctx.waitUntil(env.CACHE.put(cacheKey, JSON.stringify(data), { expirationTtl: 5 * 60 }));
    return data;
}

export async function getRelease(reqData: MSIXRequestData, requireReg: boolean): Promise<Release | Response> {
    const { 
        request, env, ctx,
        product, versionOrChannelName, reg 
    } = reqData;

    const ratatoskr = wrapRatatoskrBinding(env.RATATOSKR, request);

    const client = createClient({
        projectId: "xk715cpi",
        dataset: "production",
        useCdn: true,
        apiVersion: "2023-09-20",
        token: env.SANITY_PRODUCTS_TOKEN,
        perspective: "published"
    });

    const release = await getWithKVCache(async () => {
        // TODO: channel >=, not ==
        const release = await client.fetch<Release>(`*[_type == "release" && product->identity.current == "${product}" && (versionName == "${versionOrChannelName}" || channel == "${versionOrChannelName}")]{
            "product": product->{ "identity": identity.current },
            title,
            channel,
            version,
            versionName,
            deployments
          } | order(version desc)[0]`);
        return release ?? undefined; // Don't cache if there isn't a matching release
    }, env, ctx, "Release::" + product + "::" + versionOrChannelName);

    if (!release) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    const skuRequirement = getSkuRequirement(release);
    if (skuRequirement) {
        release.authRequired = true;
        if (reg) {
            const registration = await getWithKVCache(async () => {
                const registrationResult = await ratatoskr.getSoftwareRegistration({ token: reg });
                if (!registrationResult.success) return undefined;
                return registrationResult.data;
            }, env, ctx, "Registration::" + reg);
            if (!registration) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=401");
            if (!registration.skus.find(sku => skuRequirement.includes(sku))) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=403");
        } else if (!requireReg) {
            const licenses = await ratatoskr.getLicenses();
            if (licenses.headers) ctx.headersPatch.current = licenses.headers;
            if (!licenses.success) {
                const sidURL = new URL("https://id.jottocraft.com/sid");
                sidURL.searchParams.set("rp", request.url);
                return new Response(null, { status: 302, headers: { Location: sidURL.href } });
            }
            if (!licenses.data.find(license => skuRequirement.includes(license.sku))) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=403");
        } else {
            return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=401"); 
        }
    } else {
        release.authRequired = false;
    }

    return release;
}