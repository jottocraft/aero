// Aero
// Copyright (c) jottocraft 2023

import mime from "mime";
import { createClient } from "@sanity/client";

import { isSafe } from "../util/isSafe";
import { serveR2 } from "../util/r2";

export function expoRouter(request: Request, env: Env, ctx: AeroCtx, product: string, pathname: string): Promise<Response> {
    const [, resource, filename] = pathname.match(/\/(.+?)(?:$|\/(.*?)(?:$|\/))/) ?? [];

    switch (resource) {
        case "protocol1": return serveExpoProtocol1(request, env, ctx, product, pathname);
        case "assets": return serveExpoAsset(request, env, ctx, product, filename);
        default: return serveExpoManifest(request, env, ctx, product, resource);
    }
}

const EXPO_CHANNEL_NAMES = {
    staging: 4,
    techpreview: 3,
    alpha: 2,
    beta: 1,
    production: 0
};

async function serveExpoProtocol1(request: Request, env: Env, ctx: AeroCtx, product: string, pathname: string) {
    const url = new URL(request.url);

    const platform = request.headers.get("Expo-Platform") ?? url.searchParams.get("platform");
    const runtime = request.headers.get("Expo-Runtime-Version") ?? url.searchParams.get("runtime-version");
    const channel = request.headers.get("Expo-Channel-Name") ?? url.searchParams.get("channel-name");

    if ((platform !== "android" && platform !== "ios") || !runtime || !isSafe(runtime) || !channel || !Object.keys(EXPO_CHANNEL_NAMES).includes(channel)) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=400");

    const client = createClient({
        projectId: "xk715cpi",
        dataset: "production",
        useCdn: true,
        apiVersion: "2023-09-20",
        token: env.SANITY_PRODUCTS_TOKEN,
        perspective: "published"
    });

    const updateData = await client.fetch<UpdateData | null>(`*[_type == "release" && product->identity.current == $product && channel <= $channel && count(artifacts[_type == "expo" && platform == $platform && runtime == $runtime]) == 1] {
        "manifest": select(artifacts[_type == "expo" && platform == $platform && runtime == $runtime])[0].manifest,
        version,
        versionName,
        "changelog": changelog[]{ summary, type },
        critical
    } | order(version desc)[0]`, {
        product,
        platform,
        runtime,
        channel: EXPO_CHANNEL_NAMES[channel as keyof typeof EXPO_CHANNEL_NAMES]
    });

    if (!updateData?.manifest) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    return serveExpoManifest(request, env, ctx, product, updateData.manifest, channel, updateData);
}

async function serveExpoAsset(request: Request, env: Env, ctx: AeroCtx, product: string, filename: string) {
    if (!isSafe(filename)) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=400");
    return serveR2(request, env, ctx, `${product}/expo/${filename}`, "public, max-age=31536000, immutable", mime.getType(filename.slice(filename.indexOf(".") + 1)) ?? "");
}

async function serveExpoManifest(request: Request, env: Env, ctx: AeroCtx, product: string, manifestID: string, channel?: string, updateData?: UpdateData) {
    if (!isSafe(manifestID)) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=400");

    const object = await env.ARTIFACTS.get(`${product}/expo/${manifestID}.json`);
    if (!object) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");
    const buildManifest = await object.json<any>();

    // Add URLs to the manifest
    buildManifest.launchAsset.url = `https://aero.jottocraft.com/${product}/expo/assets/${buildManifest.launchAsset.key}.js`;
    buildManifest.assets.forEach((asset: any) => asset.url = `https://aero.jottocraft.com/${product}/expo/assets/${asset.key}${asset.fileExtension}`);

    // Preserve native build and channel headers
    if (!buildManifest.extra.expoClient.updates.requestHeaders) buildManifest.extra.expoClient.updates.requestHeaders = {};
    if (request.headers.get("Expo-Channel-Name")) buildManifest.extra.expoClient.updates.requestHeaders["Expo-Channel-Name"] = request.headers.get("Expo-Channel-Name");
    if (request.headers.get("jottocraft-Native-App-Version")) buildManifest.extra.expoClient.updates.requestHeaders["jottocraft-Native-App-Version"] = request.headers.get("jottocraft-Native-App-Version");

    // Add metadata
    if (!buildManifest.metadata) buildManifest.metadata = {};
    if (channel) buildManifest.metadata.channel = channel;
    if (updateData) buildManifest.metadata.critical = updateData.critical;
    if (updateData?.changelog) buildManifest.metadata.changelog = updateData.changelog;

    const boundary = "jottocraft-Aero-Boundary-" + crypto.randomUUID();

    let multipartBody = [];
    multipartBody.push("--" + boundary);
    multipartBody.push("Content-Disposition: inline; name=\"manifest\"");
    multipartBody.push("Content-Type: application/json\n");
    multipartBody.push("");
    multipartBody.push(JSON.stringify(buildManifest));

    multipartBody.push("--" + boundary + "--");
    multipartBody.push("");

    const headers = new Headers({
        "Content-Type": "multipart/mixed; boundary=" + boundary,
        "Cache-Control": "cache-control: private, max-age=0",
        "Expo-Protocol-Version": "1",
        "Expo-SFV-Version": "0"
    });

    if (channel) headers.set("Expo-Manifest-Filters", `channel="${channel}"`);

    return new Response(multipartBody.join("\r\n"), {
        status: 200,
        headers
    })
}