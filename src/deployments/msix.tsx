// Aero
// Copyright (c) jottocraft 2023

import React from "react";
import { renderToStaticMarkup } from "react-dom/server";
import * as htmlparser2 from "htmlparser2";
import * as DU from "domutils";
import { wrapRatatoskrBinding } from "@jottocraft/ratatoskr-sdk";
import { serveR2 } from "../util/r2";
import { getRelease } from "../releases";

// Loosely-typed .appinstaller schema factories
const AppInstaller = (props: any) => React.createElement("AppInstaller", props);
const Bundle = (props: any) => React.createElement("Bundle", props);
const Dependencies = (props: any) => React.createElement("Dependencies", props);
const MainBundle = (props: any) => React.createElement("MainBundle", props);
const Package = (props: any) => React.createElement("Package", props);
const UpdateSettings = (props: any) => React.createElement("UpdateSettings", props);
const OnLaunch = (props: any) => React.createElement("OnLaunch", props);
const AutomaticBackgroundTask = (props: any) => React.createElement("AutomaticBackgroundTask", props);

const SAFE_CHARS = /^[A-Za-z0-9-_\.]*$/;

export function msixRouter(request: Request, env: Env, ctx: AeroCtx, product: string, pathname: string): Promise<Response> {
    const url = new URL(request.url);
    const [, versionOrChannelName, fileFolder, arg] = pathname.match(/\/(.+?)\/(.+?)(?:$|\/)(.*)/) ?? [];
    const reg = url.searchParams.get("reg") ?? undefined;

    if (!versionOrChannelName || !fileFolder) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");
    if (!SAFE_CHARS.test(product) || !SAFE_CHARS.test(versionOrChannelName)) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=400");

    const requestData: MSIXRequestData = {
        request,
        url,
        env,
        ctx,
        product,
        versionOrChannelName,
        artifact: fileFolder,
        arg,
        reg
    };

    switch (fileFolder) {
        // MSIX
        case "ms-appinstaller": return serveMsAppInstaller(requestData);
        case product + ".appinstaller": return serveAppinstaller(requestData);
        case product + ".msixbundle": return serveMsixBundle(requestData);
        case "Dependencies": return serveDependency(requestData);

        default: return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");
    }
}

export async function serveAppinstaller(reqData: MSIXRequestData) {
    const { 
        url, env,
        versionOrChannelName, reg
    } = reqData;

    const release = await getRelease(reqData, true);
    if (release instanceof Response) return release;

    const product = release.product.identity;
    const flightRoot = `${url.origin}/${product}/${versionOrChannelName}`;
    const releaseRoot = `${url.origin}/${product}/${release.versionName}`;
    const regSuffix = reg ? "?reg=" + reg : "";

    const deployment = release.deployments.find(deployment => deployment._type === "msix");
    if (!deployment || (deployment._type !== "msix")) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    // Read the uploaded appinstaller
    const object = await env.ARTIFACTS.get(`${product}/msix/${deployment.folder}/${product}.appinstaller`);
    if (!object) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=503");
    const appinstallerDocument = htmlparser2.parseDocument(await object.text());

    const appinstaller = DU.findOne(e => DU.getName(e) === "appinstaller", DU.getChildren(appinstallerDocument))!;
    const mainbundle = DU.findOne(e => DU.getName(e) === "mainbundle", DU.getChildren(appinstaller))!;
    const dependencies = DU.findOne(e => DU.getName(e) === "dependencies", DU.getChildren(appinstaller))!;

    return new Response(`<?xml version="1.0" encoding="UTF-8"?>\n` + renderToStaticMarkup(
        <AppInstaller
            Uri={`${flightRoot}/${product}.appinstaller` + regSuffix}
            Version={DU.getAttributeValue(appinstaller, "version")}
            xmlns="http://schemas.microsoft.com/appx/appinstaller/2018"
        >
            <MainBundle
                Name={DU.getAttributeValue(mainbundle, "name")}
                Version={DU.getAttributeValue(mainbundle, "version")}
                Publisher={DU.getAttributeValue(mainbundle, "publisher")}
                Uri={`${releaseRoot}/${product}.msixbundle` + regSuffix}
             />
             <Dependencies>
                {DU.findAll(e => ["package", "bundle"].includes(DU.getName(e)), DU.getChildren(dependencies)).map(dependency => {    
                    const depUri = new URL(DU.getAttributeValue(dependency, "uri")!);
                    const filename = depUri.pathname.split(`/${deployment.folder}/Dependencies/`)[1];

                    const props: { [key: string]: string; } = {
                        Name: DU.getAttributeValue(dependency, "name")!,
                        Publisher: DU.getAttributeValue(dependency, "publisher")!,
                        Version: DU.getAttributeValue(dependency, "version")!,
                        Uri: `${releaseRoot}/dependencies/${filename}` + regSuffix
                    };

                    if (DU.getName(dependency) === "bundle") return <Bundle {...props} />;

                    const arch = DU.getAttributeValue(dependency, "processorarchitecture");
                    if (arch) props.ProcessorArchitecture = arch;
                    return <Package {...props} />;
                })}
             </Dependencies>
             {/* TODO: At some point this should be configurable per-product (these settings are designed for prism) */}
             <UpdateSettings>
                <OnLaunch HoursBetweenUpdateChecks="0" ShowPrompt="false" UpdateBlocksActivation="false" />
                <AutomaticBackgroundTask />
             </UpdateSettings>
        </AppInstaller>
    ), {
        status: 200,
        headers: {
            "Content-Type": "application/octet-stream"
        }
    });
}

export async function serveMsixBundle(reqData: MSIXRequestData) {
    const { env } = reqData;
    const release = await getRelease(reqData, true);
    if (release instanceof Response) return release;

    const deployment = release.deployments.find(deployment => deployment._type === "msix");
    if (!deployment || (deployment._type !== "msix")) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    return serveR2(reqData.request, reqData.env, reqData.ctx, `${release.product.identity}/msix/${deployment.folder}/${release.product.identity}.msixbundle`);
}

export async function serveDependency(reqData: MSIXRequestData) {
    const { env, arg } = reqData;
    const release = await getRelease(reqData, true);
    if (release instanceof Response) return release;

    const deployment = release.deployments.find(deployment => deployment._type === "msix");
    if (!deployment || (deployment._type !== "msix")) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");

    return serveR2(reqData.request, reqData.env, reqData.ctx, `${release.product.identity}/msix/${deployment.folder}/Dependencies/${arg}`);
}

export async function serveMsAppInstaller(reqData: MSIXRequestData) {
    const { url, env, reg: requestedReg, versionOrChannelName } = reqData;
    const release = await getRelease(reqData, false);
    if (release instanceof Response) return release;

    const ratatoskr = wrapRatatoskrBinding(env.RATATOSKR, reqData.request);
    let reg = requestedReg;

    if (!reg && release.authRequired) {
        const newReg = await ratatoskr.createSoftwareRegistration();
        if (!newReg.success) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=500");
        reg = newReg.data;
    }

    const msAppInstaller = new URL("ms-appinstaller:");
    msAppInstaller.searchParams.set("source", `${url.origin}/${release.product.identity}/${versionOrChannelName}/${release.product.identity}.appinstaller${reg ? "?reg=" + reg : ""}`);

    return new Response(null, {
        status: 302,
        headers: {
            Location: msAppInstaller.href
        }
    });
}