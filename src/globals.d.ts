// Aero
// Copyright (c) jottocraft 2023

declare type MSIXRequestData = {
	request: Request;
	url: URL;
	env: Env;
	ctx: AeroCtx;
	product: string;
	versionOrChannelName: string;
	artifact: string;
	arg?: string;
	reg?: string;
};

declare type UpdateData = {
	manifest: string;
	version: number;
	versionName: string;
	changelog: { summary: string; type: string; }[];
	critical: boolean;
};

declare type AeroCtx = ExecutionContext & {
	headersPatch: HeadersRef;
};

declare type Env = {
	ARTIFACTS: R2Bucket;
	RATATOSKR: Fetcher;
	HTTP_STATUS_PAGES: Fetcher;
	SANITY_PRODUCTS_TOKEN: string;
	CACHE: KVNamespace;
};

declare type Deployment = {
	_type: "squirrel";
	RELEASES: string;
} | {
	_type: "msix";
	folder: string;
} | {
	_type: "apk";
	package: string;
};

declare type Release = {
	product: {
		identity: string;
	};
	title: string;
	channel: string;
	version: number;
	versionName: string;
	deployments: Deployment[];
	authRequired: boolean;
};

declare type HeadersRef = { 
	current: null | RatatoskrHeaderModifications; 
};