// Aero
// Copyright (c) jottocraft 2023

import { applyModifications } from "@jottocraft/ratatoskr-sdk/lib/headers";

import { expoRouter } from "./deployments/expo";
import { msixRouter } from "./deployments/msix";
import { isSafe } from "./util/isSafe";

export default {
	async fetch(request: Request, env: Env, cfCtx: ExecutionContext): Promise<Response> {
		const headersPatch: HeadersRef = { current: null };

		const ctx = cfCtx as AeroCtx;
		ctx.headersPatch = headersPatch;

		const response = await (async () => {
			const url = new URL(request.url);
			const [, product, artifact, pathname] = url.pathname.match(/\/(.+?)\/(.+?)(\/.*)/) ?? [];
		
			if (!product || !artifact || !isSafe(product)) return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");
		
			switch (artifact) {
				case "msix": return msixRouter(request, env, ctx, product, pathname);
				case "expo": return expoRouter(request, env, ctx, product, pathname);
				default: return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=404");
			}
		})();

		return response;

		//const modifiable = new Response(response.body, response);
		//if (headersPatch.current) applyModifications(modifiable.headers, headersPatch.current);
//
		//for (const [h, v] of modifiable.headers.entries()) console.log(h + ": " + v);
//
		//return modifiable;
	}
};
